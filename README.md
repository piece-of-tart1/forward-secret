# forward-list



## Implementation of forward_list on C++20
There are actually 2 implementations of forward-list: ordinary forward_list and intrusive forward_list. For each list
there are iterators and friendly interfaces.

A forward list is a container in C++ that represents a singly linked list. It allows for efficient insertion and
deletion of elements at the front of the list. Each element in a forward list is linked to the next element
in the list, forming a chain. However, unlike doubly linked lists, forward lists don't have links to the previous
elements, which makes them more memory-efficient.

On the other hand, an intrusive forward list is a variation of the forward list where the elements themselves 
contain the linkage information needed to form the list. This means that the elements in the list must provide a
way to link themselves together. In essence, the list doesn't store pointers to the elements; instead, the
elements themselves know how to link to each other, which can be advantageous in certain scenarios where memory
layout or performance considerations are crucial.

Intrusive forward lists offer a higher degree of control and flexibility over the linking mechanism but
come with the responsibility of managing the linkage within the elements themselves. This approach can
be particularly useful in situations where memory overhead needs to be minimized or when fine-grained
control over memory layout and access patterns is necessary. However, it also requires careful design
and implementation within the elements to ensure correct behavior and maintainability of the data structure.