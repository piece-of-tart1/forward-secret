#pragma once

#include <algorithm>
#include <concepts>
#include <cstddef>
#include <iterator>
#include <type_traits>
#include <utility>

namespace intrusive {

class default_tag;

template <typename>
class forward_list_element;

template <typename T, typename Tag>
  requires std::derived_from<T, forward_list_element<Tag>>
class forward_list;

template <typename Tag = default_tag>
class forward_list_element {
  template <typename T, typename TTag>
    requires std::derived_from<T, forward_list_element<TTag>>
  friend class forward_list;

private:
  forward_list_element* next{this};

  constexpr explicit forward_list_element(forward_list_element* next) : next(next) {}

public:
  /* Create list element */
  constexpr forward_list_element() noexcept = default;

  /* Create list element */
  constexpr forward_list_element(const forward_list_element&) noexcept : forward_list_element() {}

  /* Create list element and link to other's list */
  constexpr forward_list_element(forward_list_element&& other) noexcept : forward_list_element() {
    *this = std::move(other);
  }

  /* Unlink from current list */
  constexpr forward_list_element& operator=(const forward_list_element& other) noexcept {
    if (this != &other) {
      unlink();
      next = this;
    }
    return *this;
  }

  /* Unlink from current list and link to other's list */
  constexpr forward_list_element& operator=(forward_list_element&& other) noexcept {
    if (this != &other) {
      unlink();

      if (other.is_linked()) {
        auto* prev = get_prev(&other);
        this->next = other.next;
        prev->next = this;
      }
    }
    return *this;
  }

  /* Unlink from list */
  constexpr ~forward_list_element() noexcept {
    unlink();
  }

private:
  [[nodiscard]] static constexpr forward_list_element* find_before(forward_list_element* after,
                                                                   forward_list_element* start) noexcept {
    forward_list_element* curr = start;
    while (curr->next != after) {
      curr = curr->next;
    }
    return curr;
  }

  [[nodiscard]] static constexpr forward_list_element* get_prev(forward_list_element* after) noexcept {
    return find_before(after, after->next);
  }

  constexpr void unlink() noexcept {
    unlink_after(get_prev(this));
  }

  static constexpr void unlink_after(forward_list_element* before) noexcept {
    auto* after = before->next;
    before->next = after->next;
    after->next = after;
  }

  [[nodiscard]] constexpr bool is_linked() const noexcept {
    return next != this;
  }
};

template <typename T, typename Tag = default_tag>
  requires std::derived_from<T, forward_list_element<Tag>>
class forward_list {

  using node_t = forward_list_element<Tag>;

public:
  template <bool IsConst>
  class forward_list_iterator {
    friend class forward_list<T, Tag>;

  private:
    node_t* _elem{};

  public:
    using difference_type = ptrdiff_t;
    using value_type = std::conditional_t<IsConst, const T, T>;
    using pointer = value_type*;
    using reference = value_type&;
    using iterator_category = std::forward_iterator_tag;
    using iterator_concept = std::forward_iterator_tag;

  private:
    constexpr forward_list_iterator(node_t* elem) noexcept : _elem(elem) {} // NOLINT(*-explicit-constructor)

  public:
    constexpr forward_list_iterator() noexcept = default;

    constexpr forward_list_iterator(std::nullptr_t) noexcept = delete;

    constexpr forward_list_iterator(const forward_list_iterator& other) noexcept : _elem(other._elem) {}

    constexpr forward_list_iterator& operator=(const forward_list_iterator& other) noexcept {
      if (this != &other) {
        _elem = other._elem;
      }
      return *this;
    }

    /* cast operator to const iterator */
    constexpr operator forward_list_iterator<true>() const noexcept { // NOLINT(*-explicit-constructor)
      return {_elem};
    }

    constexpr reference operator*() const noexcept {
      return static_cast<reference>(*_elem);
    }

    constexpr pointer operator->() const noexcept {
      return static_cast<pointer>(_elem);
    }

    constexpr forward_list_iterator& operator++() noexcept {
      _elem = _elem->next;
      return *this;
    }

    constexpr forward_list_iterator operator++(int) noexcept { // NOLINT(*-dcl21-cpp)
      forward_list_iterator it = *this;
      ++(*this);
      return it;
    }

    friend constexpr bool operator==(const forward_list_iterator& left, const forward_list_iterator& right) noexcept {
      return left._elem == right._elem;
    }

    friend constexpr bool operator!=(const forward_list_iterator& left, const forward_list_iterator& right) noexcept {
      return !operator==(left, right);
    }
  };

private:
  mutable node_t _sentinel{node_t()};

public:
  using size_type = size_t;
  using iterator = forward_list_iterator<false>;
  using const_iterator = forward_list_iterator<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

public:
  constexpr forward_list() noexcept = default;

  /* Intrusive list couldn't be created via copy constructor */
  constexpr forward_list(const forward_list&) = delete;
  /* Intrusive list couldn't have copy assignment operator, because intrusive list doesn't have copy constructor */
  constexpr forward_list& operator=(const forward_list&) = delete;

  constexpr forward_list(forward_list&& other) noexcept : _sentinel(other._sentinel.next) {
    node_t::get_prev(&other._sentinel)->next = &_sentinel;
    other._sentinel.next = &other._sentinel;
  };

  constexpr forward_list& operator=(forward_list&& other) noexcept {
    if (this != &other) {
      clear();
      _sentinel = other._sentinel.next;
      other.find_before(&other._sentinel)._elem->next = &_sentinel;
      other._sentinel.next = &other._sentinel;
    }
    return *this;
  };

  constexpr ~forward_list() noexcept = default;

  [[nodiscard]] constexpr bool empty() const noexcept {
    return !_sentinel.is_linked();
  }

  constexpr iterator before_begin() noexcept {
    return iterator(&_sentinel);
  }

  constexpr const_iterator before_begin() const noexcept {
    return const_iterator(&_sentinel);
  }

  constexpr iterator begin() noexcept {
    return iterator(_sentinel.next);
  }

  constexpr const_iterator begin() const noexcept {
    return const_iterator(_sentinel.next);
  }

  constexpr iterator end() noexcept {
    return iterator(&_sentinel);
  }

  constexpr const_iterator end() const noexcept {
    return const_iterator(&_sentinel);
  }

  [[nodiscard]] constexpr size_t size() const noexcept {
    return std::distance(begin(), end());
  }

  constexpr T& front() noexcept {
    return *begin();
  }

  constexpr const T& front() const noexcept {
    return *begin();
  }

  constexpr void push_front(T& value) noexcept {
    insert_after(before_begin(), value);
  }

  constexpr void pop_front() noexcept {
    node_t::unlink_after(&_sentinel);
  }

  constexpr iterator insert_after(const_iterator pos, T& value) noexcept {
    auto* prev = pos._elem;
    auto* next = &get_list_element(value);
    if (prev == next) { /* The same element */
      return iterator(prev);
    }
    if (next->is_linked()) { /* Release from other list with this Tag */
      node_t::get_prev(next)->next = next->next;
    }
    // link
    next->next = prev->next;
    prev->next = next;
    return iterator(prev->next);
  }

  constexpr iterator erase_after(const_iterator pos) noexcept {
    return erase_after(pos, const_iterator(pos._elem->next->next));
  }

  /* Removes the elements following first until last */
  constexpr iterator erase_after(const_iterator first, const_iterator last) noexcept {
    auto* temp = first._elem;
    while (temp->next != last._elem) {
      node_t::unlink_after(temp);
    }
    return iterator(last._elem);
  }

  constexpr void clear() noexcept {
    _sentinel.unlink();
  }

  constexpr iterator find_before(T& value) const noexcept {
    auto* elem = &get_list_element(value);
    return iterator(node_t::get_prev(elem));
  }

  constexpr iterator find(T& value) const noexcept {
    return iterator(std::find(begin(), end(), value)._elem);
  }

private:
  constexpr node_t& get_list_element(T& value) const noexcept {
    return static_cast<node_t&>(value);
  }
};

} // namespace intrusive
