#pragma once

#include <algorithm>
#include <concepts>
#include <cstddef>
#include <iterator>
#include <memory>
#include <new>
#include <type_traits>
#include <utility>

namespace forward_list_detail {

template <typename T>
struct base_node {
  base_node<T>* next{this};
};

template <typename T, bool UseCustomAlloc>
struct node : base_node<T> {
  using value_type = std::conditional_t<UseCustomAlloc, T*, T>;

public:
  value_type value;

public:
  constexpr node() = default;

  template <typename... Args>
    requires std::negation_v<std::bool_constant<UseCustomAlloc>>
  constexpr explicit node(Args&&... args) : value(std::forward<Args>(args)...) {}
};
} // namespace forward_list_detail

template <typename T, typename Allocator = std::allocator<T>>
class forward_list {
private:
  static constexpr bool USE_CUSTOM_ALLOCATOR = !std::is_same_v<Allocator, std::allocator<T>>;

private:
  using base_node_t = forward_list_detail::base_node<T>;
  using node_t = forward_list_detail::node<T, USE_CUSTOM_ALLOCATOR>;

public:
  template <bool IsConst>
  struct forward_list_iterator {
    friend class forward_list<T, Allocator>;

  private:
    base_node_t* _base_node{nullptr};

  public:
    using difference_type = std::ptrdiff_t;
    using value_type = std::conditional_t<IsConst, const T, T>;
    using pointer = value_type*;
    using reference = value_type&;
    using iterator_category = std::forward_iterator_tag;
    using iterator_concept = std::forward_iterator_tag;

  private:
    constexpr forward_list_iterator(base_node_t* base_node) noexcept
        : _base_node(base_node) {} // NOLINT(*-explicit-constructor)

  public:
    constexpr forward_list_iterator() noexcept = default;
    constexpr forward_list_iterator(const forward_list_iterator&) noexcept = default;

    /* cast operator to const iterator */
    constexpr operator forward_list_iterator<true>() const noexcept { // NOLINT(*-explicit-constructor)
      return {_base_node};
    }

    constexpr reference operator*() const noexcept {
      return get_node(_base_node)->value;
    }

    constexpr pointer operator->() const noexcept {
      return &get_node(_base_node)->value;
    }

    constexpr forward_list_iterator& operator++() {
      _base_node = _base_node->next;
      return *this;
    }

    constexpr forward_list_iterator operator++(int) { // NOLINT(*-dcl21-cpp)
      forward_list_iterator tmp = *this;
      ++(*this);
      return tmp;
    }

    friend constexpr bool operator==(const forward_list_iterator& left, const forward_list_iterator& right) noexcept {
      return left._base_node == right._base_node;
    }

    friend constexpr bool operator!=(const forward_list_iterator& left, const forward_list_iterator& right) noexcept {
      return !operator==(left, right);
    }
  };

  using value_type = T;
  using allocator_type = Allocator;
  using size_type = size_t;
  using difference_type = ptrdiff_t;
  using reference = value_type&;
  using const_reference = const value_type&;
  using pointer = std::allocator_traits<Allocator>::pointer;
  using const_pointer = std::allocator_traits<Allocator>::const_pointer;
  using iterator = forward_list_iterator<false>;
  using const_iterator = forward_list_iterator<true>;

private:
  mutable base_node_t _sentinel{base_node_t()};
  /* for O(1) move constructor/assignment */
  base_node_t* _before_sentinel{&_sentinel};
  [[no_unique_address]] Allocator _allocator;

public:
  constexpr forward_list() noexcept = default;

  constexpr explicit forward_list(const Allocator& allocator) : _allocator(allocator) {}

  constexpr forward_list(size_type count, const T& value, const Allocator& allocator = Allocator())
      : _allocator(allocator) {
    for (size_type i = 0; i < count; i++) {
      push_front(value);
    }
  }

  template <std::input_iterator InputIterator>
  constexpr forward_list(InputIterator first, InputIterator last, const Allocator& allocator = Allocator())
      : _allocator(allocator) {
    insert_after(before_begin(), first, last);
  }

  constexpr forward_list(const forward_list& other) : _allocator(other._allocator) {
    insert_after<const_iterator>(before_begin(), other.begin(), other.end());
  }

  constexpr forward_list(forward_list&& other) noexcept
      : _sentinel(other._sentinel.next),
        _allocator(std::move(other._allocator)) {
    _before_sentinel = other._before_sentinel;
    _before_sentinel->next = &_sentinel;
    other._sentinel.next = &other._sentinel;
    other._before_sentinel = &other._sentinel;
  }

  constexpr forward_list(forward_list&& other, const Allocator& allocator) noexcept
      : _sentinel(other._sentinel.next),
        _allocator(allocator) {
    _before_sentinel = other._before_sentinel;
    _before_sentinel->next = &_sentinel;
    other._sentinel.next = &other._sentinel;
    other._before_sentinel = &other._sentinel;
  }

  constexpr forward_list& operator=(const forward_list& other) {
    if (this != &other) {
      clear();
      insert_after(before_begin(), other.begin(), other.end());
    }
    return *this;
  }

  constexpr forward_list& operator=(forward_list&& other) noexcept {
    if (this != &other) {
      clear();
      _sentinel.next = other._sentinel.next;
      _before_sentinel = other._before_sentinel;
      _before_sentinel->next = &_sentinel;
      other._sentinel.next = &other._sentinel;
      other._before_sentinel = &other._sentinel;
    }
    return *this;
  }

  constexpr ~forward_list() noexcept {
    clear();
  }

  [[nodiscard]] constexpr allocator_type get_allocator() const noexcept {
    return _allocator;
  }

  [[nodiscard]] constexpr bool empty() const noexcept {
    return _sentinel.next == &_sentinel;
  }

  constexpr iterator before_begin() noexcept {
    return iterator(&_sentinel);
  }

  constexpr const_iterator before_begin() const noexcept {
    return const_iterator(&_sentinel);
  }

  constexpr iterator begin() noexcept {
    return iterator(_sentinel.next);
  }

  constexpr const_iterator begin() const noexcept {
    return const_iterator(_sentinel.next);
  }

  constexpr iterator end() noexcept {
    return iterator(&_sentinel);
  }

  constexpr const_iterator end() const noexcept {
    return const_iterator(&_sentinel);
  }

  [[nodiscard]] constexpr size_t size() const noexcept {
    return std::distance(begin(), end());
  }

  template <typename... Args>
  constexpr iterator emplace_after(const_iterator pos, Args&&... args) {
    node_t* curr = create_node(std::forward<Args>(args)...);
    link_to(curr, pos._base_node->next);
    link_to(pos._base_node, curr);
    return iterator(curr);
  }

  constexpr void push_front(const T& value) {
    insert_after(before_begin(), value);
  }

  constexpr iterator insert_after(const_iterator pos, const T& value) {
    return emplace_after(pos, value);
  }

  template <std::input_iterator InputIterator>
  constexpr iterator insert_after(const_iterator pos, InputIterator first, InputIterator last) {
    auto* p = pos._base_node;
    while (first != last) {
      insert_after(const_iterator(p), *first);
      p = p->next;
      ++first;
    }
    return iterator(p);
  }

  constexpr iterator erase_after(const_iterator pos) noexcept {
    return erase_after(pos, const_iterator(pos._base_node->next->next));
  }

  /* Removes the elements following first until last */
  constexpr iterator erase_after(const_iterator first, const_iterator last) noexcept {
    auto* temp = first._base_node;
    while (temp->next != last._base_node) {
      auto* unlinked = temp->next;
      unlink_after(temp);
      delete_node(get_node(unlinked));
    }
    return iterator(last._base_node);
  }

  constexpr void clear() noexcept {
    erase_after(before_begin(), end());
  }

  constexpr iterator find_before(const T& value) const noexcept {
    for (auto it = before_begin(); it._base_node->next != end(); ++it) {
      if (get_node(it._base_node->next)->value == value) {
        return iterator(it._base_node);
      }
    }
    return iterator(&_sentinel);
  }

  constexpr iterator find(const T& value) const noexcept {
    return iterator(std::find(begin(), end(), value)._base_node);
  }

private:
  template <typename... Args>
  constexpr node_t* create_node(Args&&... args) const {
    if constexpr (USE_CUSTOM_ALLOCATOR) {
      auto* node = new node_t();
      node->next = node;
      try {
        T* pointer = _allocator.allocate(1);
        if (pointer == nullptr) {
          throw std::bad_alloc();
        }
        node->value = pointer;
        try {
          std::construct_at(pointer, std::forward<Args>(args)...);
        } catch (...) {
          _allocator.deallocate(pointer, 1);
        }
      } catch (...) {
        delete (node);
        throw;
      }
      return node;
    } else {
      return new node_t(std::forward<Args>(args)...);
    }
  }

  constexpr void link_to(base_node_t* prev, base_node_t* next) noexcept {
    if (next == &_sentinel) {
      _before_sentinel = prev;
    }
    prev->next = next;
  }

  constexpr void delete_node(node_t* node) const noexcept {
    if constexpr (USE_CUSTOM_ALLOCATOR) {
      T* pointer = node->value;
      _allocator.deallocate(pointer, 1);
    }
    delete (node);
  }

  constexpr void unlink_after(base_node_t* before) noexcept {
    auto* after = before->next;
    link_to(before, after->next);
    link_to(after, after);
  }

  static constexpr node_t* get_node(base_node_t* node) {
    return static_cast<node_t*>(node);
  }
};