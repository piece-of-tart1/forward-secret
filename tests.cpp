#include "src/forward_list.h"
#include "src/intrusive_forward_list.h"

#include <cassert>
#include <iostream>
#include <type_traits>
#include <utility>
#include <vector>

void AssertForwardListElements(auto&& list, auto&& elem) {
  auto it = list.begin();
  auto elem_it = elem.begin();
  while (it != list.end()) {
    assert(*it == *elem_it);
    ++it;
    ++elem_it;
  }
  assert(elem_it == elem.end());
}

void BaseForwardListTest() {
  forward_list<int> a;
  a.push_front(12);
  a.push_front(13);
  a.push_front(14);

  AssertForwardListElements(a, std::vector<int>{14, 13, 12});
}

void CopyConstructorTest() {
  forward_list<int> a;
  a.push_front(12);
  a.push_front(13);
  a.push_front(14);

  auto b = a;

  AssertForwardListElements(a, std::vector<int>{14, 13, 12});
  AssertForwardListElements(b, std::vector<int>{14, 13, 12});
}

void EraseForwardListTest() {
  forward_list<int> a;
  a.push_front(12);
  a.push_front(13);
  a.push_front(14);
  a.push_front(15);

  a.erase_after(std::next(a.before_begin(), 3));
  assert(a.size() == 3);
  AssertForwardListElements(a, std::vector<int>{15, 14, 13});
  a.erase_after(a.before_begin(), std::next(a.begin(), 2));
  assert(a.size() == 1);
  AssertForwardListElements(a, std::vector<int>{13});
}

void CountConstructorForwardListTest() {
  forward_list<int> a(10, -101);
  AssertForwardListElements(a, std::vector(10, -101));
}

void GetAllocatorTest() {
  forward_list<int> a;
  assert(a.get_allocator() == std::allocator<int>());
}

void MoveConstructorTest() {
  forward_list<int> a(5, 24, std::allocator<int>());
  forward_list<int> b(std::move(a), a.get_allocator());
  assert(a.empty());
  assert(a.begin() == a.end());
  AssertForwardListElements(b, std::vector<int>(5, 24));
}

void MoveAssignmentOperatorTest() {
  forward_list<int> a(50, 808);
  forward_list<int> b(100, 909);
  b = std::move(a);
  assert(a.empty());
  assert(a.begin() == a.end());
  AssertForwardListElements(b, std::vector<int>(50, 808));
}

void InsertAfterForwardListTest() {
  forward_list<int> a;
  a.insert_after(a.before_begin(), 1);
  a.insert_after(std::next(a.before_begin(), 1), 2);
  a.insert_after(std::next(a.before_begin(), 2), 3);
  AssertForwardListElements(a, std::vector<int>{1, 2, 3});
}

void UsingForwardListTest() {
  using list_t = forward_list<int>;
  static_assert(std::is_same_v<list_t::value_type, int>);
  static_assert(std::is_same_v<list_t::allocator_type, std::allocator<int>>);
  static_assert(std::is_same_v<list_t::size_type, size_t>);
  static_assert(std::is_same_v<list_t::difference_type, ptrdiff_t>);
  static_assert(std::is_same_v<list_t::reference, int&>);
  static_assert(std::is_same_v<list_t::const_reference, const int&>);
  static_assert(std::is_same_v<list_t::pointer, int*>);
  static_assert(std::is_same_v<list_t::const_pointer, const int*>);
  static_assert(std::is_same_v<list_t::iterator, list_t::forward_list_iterator<false>>);
  static_assert(std::is_same_v<list_t::const_iterator, list_t::forward_list_iterator<true>>);

  using it_t = list_t::iterator;
  static_assert(std::is_same_v<it_t::difference_type, ptrdiff_t>);
  static_assert(std::is_same_v<it_t::value_type, int>);
  static_assert(std::is_same_v<it_t::pointer, int*>);
  static_assert(std::is_same_v<it_t::reference, int&>);
  static_assert(std::is_same_v<it_t::iterator_category, std::forward_iterator_tag>);
  static_assert(std::is_same_v<it_t::iterator_concept, std::forward_iterator_tag>);
}

void FindForwardListTest() {
  forward_list<int> a;
  for (int i = 100; i >= 0; i--) {
    a.push_front(i);
  }
  for (int i = 100; i > 0; i--) {
    assert(*a.find_before(i) == i - 1);
  }
  for (int i = 100; i >= 0; i--) {
    assert(*a.find(i) == i);
  }
}

struct TestTag {};

struct NodeStruct : intrusive::forward_list_element<TestTag> {
  int value;

  NodeStruct(int value) : value(value) {} // NOLINT(*-explicit-constructor)

  friend bool operator==(const NodeStruct& left, const NodeStruct& right) noexcept {
    return left.value == right.value;
  }

  friend bool operator!=(const NodeStruct& left, const NodeStruct& right) noexcept {
    return left.value != right.value;
  }
};

void BaseIntrusiveListTest() {
  intrusive::forward_list<NodeStruct, TestTag> list;
  NodeStruct node1(1);
  NodeStruct node2(2);
  NodeStruct node3(3);
  list.push_front(node1);
  list.push_front(node2);
  list.push_front(node3);

  AssertForwardListElements(list, std::vector<NodeStruct>{3, 2, 1});
}

void MoveIntrusiveListTest() {
  intrusive::forward_list<NodeStruct, TestTag> a;
  std::vector<NodeStruct> nodes;
  nodes.reserve(101);
  for (int i = 0; i < 100; i++) {
    nodes.emplace_back(100 - i);
    a.push_front(nodes[i]);
  }

  auto b = std::move(a);
  assert(a.empty());
  assert(a.size() == 0); // NOLINT(*-container-size-empty)
  assert(a.begin() == a.end());
  assert(a.before_begin() == a.end());

  AssertForwardListElements(b, std::vector<NodeStruct>(nodes.rbegin(), nodes.rend()));
}

void EraseIntrusiveForwardListTest() {
  intrusive::forward_list<NodeStruct, TestTag> a;
  std::vector<NodeStruct> nodes;
  nodes.reserve(101);
  for (int i = 0; i < 100; i++) {
    nodes.emplace_back(100 - i);
    a.push_front(nodes[i]);
  }
  for (int i = 0; i < 30; i++) {
    a.pop_front();
  }

  {
    auto v = std::vector<NodeStruct>(std::next(nodes.rbegin(), 30), nodes.rend());
    AssertForwardListElements(a, v);
  }

  a.erase_after(std::next(a.before_begin(), 5), a.end());
  AssertForwardListElements(a, std::vector<NodeStruct>{31, 32, 33, 34, 35});
}

void EraseViaLifetimeIntrusiveForwardListTest() {
  intrusive::forward_list<NodeStruct, TestTag> list;
  {
    NodeStruct node1(1);
    {
      NodeStruct node2(2);
      {
        NodeStruct node3(3);
        list.push_front(node1);
        list.push_front(node2);
        list.push_front(node3);
        assert(list.size() == 3);
      }
      assert(list.size() == 2);
    }
    assert(list.size() == 1);
  }
  assert(list.empty());
}

void FindIntrusiveForwardListTest() {
  intrusive::forward_list<NodeStruct, TestTag> a;
  std::vector<NodeStruct> nodes;
  nodes.reserve(101);
  for (int i = 0; i < 100; i++) {
    nodes.emplace_back(100 - i);
    a.push_front(nodes[i]);
  }
  for (int i = 99; i > 0; i--) {
    assert(*a.find_before(nodes[i - 1]) == NodeStruct{100 - i});
  }
  for (int i = 99; i >= 0; i--) {
    assert(*a.find(nodes[i]) == NodeStruct{100 - i});
  }
}

void UsingIntrusiveForwardListTest() {
  using list_t = intrusive::forward_list<NodeStruct, TestTag>;
  static_assert(std::is_same_v<list_t::size_type, size_t>);
  static_assert(std::is_same_v<list_t::iterator, list_t::forward_list_iterator<false>>);
  static_assert(std::is_same_v<list_t::const_iterator, list_t::forward_list_iterator<true>>);
  static_assert(std::is_same_v<list_t::reverse_iterator, std::reverse_iterator<list_t::iterator>>);
  static_assert(std::is_same_v<list_t::const_reverse_iterator, std::reverse_iterator<list_t::const_iterator>>);

  using it_t = list_t::iterator;
  static_assert(std::is_same_v<it_t::difference_type, ptrdiff_t>);
  static_assert(std::is_same_v<it_t::value_type, NodeStruct>);
  static_assert(std::is_same_v<it_t::pointer, NodeStruct*>);
  static_assert(std::is_same_v<it_t::reference, NodeStruct&>);
  static_assert(std::is_same_v<it_t::iterator_category, std::forward_iterator_tag>);
  static_assert(std::is_same_v<it_t::iterator_concept, std::forward_iterator_tag>);
}

int main() {
  BaseForwardListTest();
  CopyConstructorTest();
  EraseForwardListTest();
  GetAllocatorTest();
  CountConstructorForwardListTest();
  MoveConstructorTest();
  MoveAssignmentOperatorTest();
  InsertAfterForwardListTest();
  FindForwardListTest();
  UsingForwardListTest();
  std::cout << "forward_list tests are done!" << std::endl;

  BaseIntrusiveListTest();
  MoveIntrusiveListTest();
  EraseIntrusiveForwardListTest();
  EraseViaLifetimeIntrusiveForwardListTest();
  FindIntrusiveForwardListTest();
  UsingIntrusiveForwardListTest();

  std::cout << "intrusive forward_list tests are done!" << std::endl;
  return EXIT_SUCCESS;
}
